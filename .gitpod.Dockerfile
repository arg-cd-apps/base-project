FROM gitpod/workspace-node-lts

ENV DOTNET_ROOT="/tmp/.dotnet"
RUN mkdir -p "$DOTNET_ROOT" && chown gitpod:gitpod $DOTNET_ROOT
USER gitpod

#https://github.com/CycloneDX/cyclonedx-cli/commit/1235e6cc38f98385c03b827a7a70ebd7b297fd7a?diff=split
# messy handling for https://github.com/gitpod-io/gitpod/issues/5090
ENV PATH=$PATH:$DOTNET_ROOT
ENV DOTNET_CLI_TELEMETRY_OPTOUT=true

# Install .NET SDK (Current channel)
# Source: https://docs.microsoft.com/dotnet/core/install/linux-scripted-manual#scripted-install
RUN wget --output-document="$DOTNET_ROOT/dotnet-install.sh" https://dot.net/v1/dotnet-install.sh \
    && chmod +x "$DOTNET_ROOT/dotnet-install.sh"
RUN "$DOTNET_ROOT/dotnet-install.sh" --channel 6.0 --install-dir "$DOTNET_ROOT"
